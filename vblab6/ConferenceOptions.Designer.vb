﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConferenceOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpConference = New System.Windows.Forms.GroupBox()
        Me.chkOpeningNight = New System.Windows.Forms.CheckBox()
        Me.chkConferenceReg = New System.Windows.Forms.CheckBox()
        Me.grpPreconference = New System.Windows.Forms.GroupBox()
        Me.lblSelect = New System.Windows.Forms.Label()
        Me.lstPreconference = New System.Windows.Forms.ListBox()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.grpConference.SuspendLayout()
        Me.grpPreconference.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpConference
        '
        Me.grpConference.Controls.Add(Me.chkOpeningNight)
        Me.grpConference.Controls.Add(Me.chkConferenceReg)
        Me.grpConference.Location = New System.Drawing.Point(12, 36)
        Me.grpConference.Name = "grpConference"
        Me.grpConference.Size = New System.Drawing.Size(287, 186)
        Me.grpConference.TabIndex = 0
        Me.grpConference.TabStop = False
        Me.grpConference.Text = "Conference"
        '
        'chkOpeningNight
        '
        Me.chkOpeningNight.AutoSize = True
        Me.chkOpeningNight.Location = New System.Drawing.Point(26, 84)
        Me.chkOpeningNight.Name = "chkOpeningNight"
        Me.chkOpeningNight.Size = New System.Drawing.Size(205, 17)
        Me.chkOpeningNight.TabIndex = 1
        Me.chkOpeningNight.Text = "Opening Night Dinner && KeyNote: $30"
        Me.chkOpeningNight.UseVisualStyleBackColor = True
        '
        'chkConferenceReg
        '
        Me.chkConferenceReg.AutoSize = True
        Me.chkConferenceReg.Location = New System.Drawing.Point(26, 50)
        Me.chkConferenceReg.Name = "chkConferenceReg"
        Me.chkConferenceReg.Size = New System.Drawing.Size(170, 17)
        Me.chkConferenceReg.TabIndex = 0
        Me.chkConferenceReg.Text = "Conference Registration: $895"
        Me.chkConferenceReg.UseVisualStyleBackColor = True
        '
        'grpPreconference
        '
        Me.grpPreconference.Controls.Add(Me.lblSelect)
        Me.grpPreconference.Controls.Add(Me.lstPreconference)
        Me.grpPreconference.Location = New System.Drawing.Point(305, 36)
        Me.grpPreconference.Name = "grpPreconference"
        Me.grpPreconference.Size = New System.Drawing.Size(267, 186)
        Me.grpPreconference.TabIndex = 1
        Me.grpPreconference.TabStop = False
        Me.grpPreconference.Text = "Preconference Workshops"
        '
        'lblSelect
        '
        Me.lblSelect.AutoSize = True
        Me.lblSelect.Location = New System.Drawing.Point(29, 26)
        Me.lblSelect.Name = "lblSelect"
        Me.lblSelect.Size = New System.Drawing.Size(60, 13)
        Me.lblSelect.TabIndex = 1
        Me.lblSelect.Text = "Select One"
        '
        'lstPreconference
        '
        Me.lstPreconference.FormattingEnabled = True
        Me.lstPreconference.Items.AddRange(New Object() {"Introduction to E-commerce", "The Future of the Web", "Advanced Visual Basic", "Network Security"})
        Me.lstPreconference.Location = New System.Drawing.Point(32, 55)
        Me.lstPreconference.Name = "lstPreconference"
        Me.lstPreconference.Size = New System.Drawing.Size(208, 108)
        Me.lstPreconference.TabIndex = 0
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(350, 269)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 23)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(462, 269)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ConferenceOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 318)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.grpPreconference)
        Me.Controls.Add(Me.grpConference)
        Me.Name = "ConferenceOptions"
        Me.Text = "ConferenceOptions"
        Me.grpConference.ResumeLayout(False)
        Me.grpConference.PerformLayout()
        Me.grpPreconference.ResumeLayout(False)
        Me.grpPreconference.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpConference As System.Windows.Forms.GroupBox
    Friend WithEvents grpPreconference As System.Windows.Forms.GroupBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents chkOpeningNight As System.Windows.Forms.CheckBox
    Friend WithEvents chkConferenceReg As System.Windows.Forms.CheckBox
    Friend WithEvents lblSelect As System.Windows.Forms.Label
    Friend WithEvents lstPreconference As System.Windows.Forms.ListBox
End Class
