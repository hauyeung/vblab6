﻿Public Class ConferenceOptions

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        chkConferenceReg.Checked = False
        chkOpeningNight.Checked = False
        lstPreconference.ClearSelected()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

        Close()
    End Sub

    Private Sub ConferenceOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load        
        chkConferenceReg.Checked = False
        chkOpeningNight.Checked = False
    End Sub

End Class