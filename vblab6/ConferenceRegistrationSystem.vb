﻿Imports System.Text.RegularExpressions
Public Class ConferenceRegistrationSystem
    Private confreg As Boolean = False
    Private opennight As Boolean = False
    Private preconfworkshops As ListBox.SelectedObjectCollection
    Private total As Double    
    Private confoptions As New ConferenceOptions
    Private chosen As Boolean = False
    Private Sub btnSet_Click(sender As Object, e As EventArgs) Handles btnSet.Click
        If Regex.IsMatch(txtEmail.Text, "[0-9a-z_.\-]+@[0-9a-z]+.[0-9a-z]+") = False Then
            MessageBox.Show("Please enter a valid email address")
        End If
        If Regex.IsMatch(txtPhone.Text, "[0-9\-]{12}") = False Then
            MessageBox.Show("Please enter a valid phone number")
        End If
        Try
            Integer.TryParse(txtZip.Text, Globalization.NumberStyles.Integer)
        Catch ex As Exception
            MessageBox.Show("Please enter a valid zip code")
        End Try
        Dim valid As Boolean = False
        valid = Regex.IsMatch(txtEmail.Text, "[0-9a-z_.\-]+@[0-9a-z]+.[0-9a-z]+") And Regex.IsMatch(txtPhone.Text, "[0-9\-]{12}")
        If valid Then
            confoptions.ShowDialog()
            confreg = confoptions.chkConferenceReg.Checked
            opennight = confoptions.chkOpeningNight.Checked
            If confreg Then
                total += 895 + getpreconfworkshopprice(confoptions.lstPreconference.SelectedItem.ToString())
            End If
            If opennight And confreg Then
                total += 30
            End If            
            txtTotal.Text = "$" + total.ToString()
        End If

    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        For Each TextBox In grpRegistrant.Controls
            If TypeOf TextBox Is TextBox Then
                TextBox.text = ""
            End If
        Next
        txtTotal.Text = String.Empty
        chosen = False        
        total = 0
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub ConferenceRegistrationSystem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtState.MaxLength = 2
        txtZip.MaxLength = 5
        txtPhone.MaxLength = 12
    End Sub

    Private Function getpreconfworkshopprice(ByRef workshop As String) As Double
        If workshop = "Introduction to E-commerce" Or workshop = "The Future of the Web" Then
            Return 295
        ElseIf workshop = "Advanced Visual Basic" Or workshop = "Network Security" Then
            Return 395
        End If
        Return 0
    End Function
End Class
